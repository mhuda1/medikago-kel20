from django.shortcuts import render, redirect, HttpResponse
from django.contrib.auth.forms import UserCreationForm
# Create your views here.
from django.http import QueryDict
from .models import *


def login(request):
    data = {}
    if(request.method == 'GET'):
        if (request.session.get('user') is None):
            return render(request, 'registration/login.html', data)
        else:
            return redirect('pages:profil')
    else:
        data['username'] = request.POST['username']
        data['password'] = request.POST['password']

        if any(get_pengguna_from_specific_table('PENGGUNA', data['username'])):
            pengguna = get_pengguna_from_specific_table(
                'PENGGUNA', data['username'])[0]
            if data['password'] == pengguna['password']:

                if any(get_pengguna_from_specific_table('administrator', data['username'])):
                    pengguna['role'] = 'admin'
                elif any(get_pengguna_from_specific_table('dokter', data['username'])):
                    pengguna['role'] = 'pengguna'
                elif any(get_pengguna_from_specific_table('pasien', data['username'])):
                    pengguna['role'] = 'pengguna'
                else:
                    pengguna['role'] = 'not_set'
                print(pengguna)
                filtered_pengguna = {}
                for k, v in pengguna.items():
                    if k != 'tanggal_lahir':
                        filtered_pengguna[k] = v
                print(filtered_pengguna)
                request.session['user'] = filtered_pengguna
                return redirect('pages:profil')

            else:
                return render(request, 'registration/login.html', data)
        else:
            return render(request, 'registration/login.html', data)


def logout(request):
    request.session.flush()
    return redirect('pages:home')


def roles(request):
    return render(request, 'roles.html')


def registerAdmin(request):
    data = {}
    if request.method == "GET":
        if (request.session.get('user') is None):
            return render(request, 'registration/registerAdmin.html', data)
        else:
            return redirect('pages:profil')
    else:
        current_id = get_biggest_id_on_specific_table('admin')
        current_id_angka = current_id.split('.')
        angka_increment = str(int(current_id_angka[0]) + 1)
        new_nomor_pegawai = angka_increment + \
            '.' + current_id_angka[1]

        data['email'] = request.POST['email']
        data['username'] = request.POST['username']
        data['password'] = request.POST['password']
        data['nama_lengkap'] = request.POST['nama']
        data['nomor_id'] = request.POST['id']
        data['tanggal_lahir'] = request.POST['tanggal_lahir']
        data['alamat'] = request.POST['alamat']
        data['nomor_pegawai'] = new_nomor_pegawai
        data['kode_rs'] = '1.0'

        if(any(get_pengguna_from_specific_table('PENGGUNA', data['username'])) or any(get_email_from_specific_table('PENGGUNA', data['email']))):
            return render(request, 'registration/registerAdmin.html', data)
        else:
            # print(data)
            insert_pengguna_with_role('admin', data)
            pengguna = get_pengguna_from_specific_table(
                'administrator', data['username'])[0]
            pengguna['role'] = 'admin'
            request.session['user'] = pengguna
            return redirect('pages:profil')


def registerDokter(request):
    data = {}
    if request.method == "GET":
        if (request.session.get('user') is None):
            return render(request, 'registration/registerDokter.html', data)
        else:
            return redirect('pages:profil')
    else:
        current_id = get_biggest_id_on_specific_table('dokter')
        current_id_angka = current_id.split('.')
        angka_increment = str(int(current_id_angka[0]) + 1)
        new_id_dokter = angka_increment + \
            '.' + current_id_angka[1]

        data['email'] = request.POST['email']
        data['username'] = request.POST['username']
        data['password'] = request.POST['password']
        data['nama_lengkap'] = request.POST['nama']
        data['nomor_id'] = request.POST['id']
        data['tanggal_lahir'] = request.POST['tanggal_lahir']
        data['alamat'] = request.POST['alamat']
        data['no_sip'] = request.POST['sip']
        data['spesialisasi'] = request.POST['spesialisasi']
        data['id_dokter'] = new_id_dokter

        if(any(get_pengguna_from_specific_table('PENGGUNA', data['username'])) or any(get_email_from_specific_table('PENGGUNA', data['email']))):
            return render(request, 'registration/registerDokter.html', data)
        else:
            # print(data)
            insert_pengguna_with_role('dokter', data)
            pengguna = get_pengguna_from_specific_table(
                'dokter', data['username'])[0]
            pengguna['role'] = 'user'
            request.session['user'] = pengguna
            return redirect('pages:profil')


def registerPasien(request):
    data = {}
    if request.method == "GET":
        if (request.session.get('user') is None):
            return render(request, 'registration/registerPasien.html', data)
        else:
            return redirect('pages:profil')
    else:
        current_id = get_biggest_id_on_specific_table('pasien')
        current_id_angka = current_id.split('.')
        angka_increment = str(int(current_id_angka[0]) + 1)
        new_no_rekam_medis = angka_increment + \
            '.' + current_id_angka[1]

        data['email'] = request.POST['email']
        data['username'] = request.POST['username']
        data['password'] = request.POST['password']
        data['nama_lengkap'] = request.POST['nama']
        data['nomor_id'] = request.POST['id']
        data['tanggal_lahir'] = request.POST['tanggal_lahir']
        data['alamat'] = request.POST['alamat']
        data['no_rekam_medis'] = new_no_rekam_medis
        if request.POST.get('alergi') != '':
            data['alergi'] = request.POST.get('alergi').split(',')
        else:
            data['alergi'] = request.POST.get('alergi')

        if(any(get_pengguna_from_specific_table('PENGGUNA', data['username'])) or any(get_email_from_specific_table('PENGGUNA', data['email']))):
            return render(request, 'registration/registerPasien.html', data)
        else:
            # print(data)
            insert_pengguna_with_role('pasien', data)
            pengguna = get_pengguna_from_specific_table(
                'pasien', data['username'])[0]
            pengguna['role'] = 'user'
            request.session['user'] = pengguna
            return redirect('pages:profil')
