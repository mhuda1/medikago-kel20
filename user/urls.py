from django.urls import path
from . import views

app_name = "user"

urlpatterns = [
    path('login/', views.login, name="login"),
    path('logout/', views.logout, name="logout"),
    path('roles/', views.roles, name="roles"),
    path('registerAdmin/', views.registerAdmin, name="registerAdmin"),
    path('registerDokter/', views.registerDokter, name="registerDokter"),
    path('registerPasien/', views.registerPasien, name="registerPasien"),
]
