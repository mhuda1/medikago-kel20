from django.db import models, connection
from .converter import dictfetchall


def fetch_all_administrator():
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM administrator")
    return dictfetchall(cursor)


def fetch_all_pengguna():
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM pengguna")
    return dictfetchall(cursor)


def get_pengguna_from_specific_table(table_name, username):
    cursor = connection.cursor()
    if str.lower(table_name) == 'pengguna':
        cursor.execute(
            "SELECT * FROM PENGGUNA WHERE username = %s", [username])
    elif str.lower(table_name) == 'administrator':
        cursor.execute(
            "SELECT * FROM ADMINISTRATOR WHERE username = %s", [username])
    elif str.lower(table_name) == 'dokter':
        cursor.execute("SELECT * FROM DOKTER WHERE username = %s", [username])
    else:
        cursor.execute("SELECT * FROM PASIEN WHERE username = %s", [username])
    pengguna = dictfetchall(cursor)
    return pengguna


def get_email_from_specific_table(table_name, email):
    cursor = connection.cursor()
    if str.lower(table_name) == 'pengguna':
        cursor.execute(
            "SELECT * FROM PENGGUNA WHERE email = %s", [email])
    elif str.lower(table_name) == 'administrator':
        cursor.execute(
            "SELECT * FROM ADMINISTRATOR WHERE email = %s", [email])
    elif str.lower(table_name) == 'dokter':
        cursor.execute("SELECT * FROM DOKTER WHERE email = %s", [email])
    else:
        cursor.execute("SELECT * FROM PASIEN WHERE email = %s", [email])
    pengguna = dictfetchall(cursor)
    return pengguna


def get_biggest_id_on_specific_table(table_name):
    cursor = connection.cursor()
    if str.lower(table_name) == 'admin':
        cursor.execute(
            "SELECT * FROM ADMINISTRATOR ORDER BY CAST(nomor_pegawai AS DOUBLE PRECISION) DESC LIMIT 1")
        admin = cursor.fetchone()
        nomor_pegawai = admin[0]
        return nomor_pegawai
    elif str.lower(table_name) == 'dokter':
        cursor.execute(
            "SELECT * FROM DOKTER ORDER BY CAST(id_dokter AS DOUBLE PRECISION) DESC LIMIT 1")
        dokter = cursor.fetchone()
        id_dokter = dokter[0]
        return id_dokter
    elif str.lower(table_name) == 'pasien':
        cursor.execute(
            "SELECT * FROM PASIEN ORDER BY CAST(no_rekam_medis AS DOUBLE PRECISION) DESC LIMIT 1")
        pasien = cursor.fetchone()
        no_rekam_medis = pasien[0]
        return no_rekam_medis
    else:
        return ''


def insert_pengguna_with_role(table_name, data):
    cursor = connection.cursor()
    cursor.execute("INSERT INTO PENGGUNA VALUES (%s, %s, %s, %s, %s, %s, %s)", [
                   data['email'], data['username'], data['password'], data['nama_lengkap'], data['nomor_id'], data['tanggal_lahir'], data['alamat']])
    if str.lower(table_name) == 'admin':
        cursor.execute("INSERT INTO ADMINISTRATOR VALUES (%s, %s, %s)", [
                       data['nomor_pegawai'], data['username'], data['kode_rs']])
        return
    elif str.lower(table_name) == 'dokter':
        cursor.execute("INSERT INTO DOKTER VALUES (%s, %s, %s, %s)", [
                       data['id_dokter'], data['username'], data['no_sip'], data['spesialisasi']])
        return
    elif str.lower(table_name) == 'pasien':
        cursor.execute("INSERT INTO PASIEN VALUES (%s, %s, 'Aviva')", [
                       data['no_rekam_medis'], data['username']])
        if data['alergi'] != '':
            for alergi in data['alergi']:
                cursor.execute("INSERT INTO ALERGI_PASIEN VALUES (%s, %s)", [
                    data['no_rekam_medis'], alergi])
        return
    else:
        return


def get_biggest_rs_id():
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM RS_CABANG ORDER BY kode_rs DESC LIMIT 1")
    produk = cursor.fetchone()
    if (produk is None):
        return '0.0'
    else:
        return produk[0]
