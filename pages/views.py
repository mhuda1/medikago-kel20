from django.shortcuts import render, redirect, HttpResponse
from .models import *
from datetime import date
from user.models import *
# Create your views here.

def index(request):
    return render(request, 'LandingPage.html')


# # # # # # # # route dashboard # # # # # # #

def dashboardAdmin(request):
    return redirect('pages:profil')


def profil(request):
    data = {}
    if(request.method == 'GET') & (request.session.get('user') is not None):
        pengguna = get_pengguna_from_specific_table(
            'PENGGUNA', request.session['user']['username'])[0]
        print(pengguna)
        return render(request, 'dashboardAdmin/Profil/profil.html', pengguna)
    else:
        return redirect('pages:home')
    return render(request, 'dashboardAdmin/Profil/profil.html')


# # # # # # # # route konsultasi # # # # # # #
def buatKonsultasi(request):
    data = {}
    if request.session.get('user') is not None:
        if (request.session['user']['role'] == 'admin'):
            if request.method == "POST":
                current_id = get_biggest_konsultasi_id()
                current_id_angka = current_id.split('.')
                angka_increment = str(int(current_id_angka[0]) + 1)
                new_sesi_konsultasi_id = angka_increment + \
                    '.' + current_id_angka[1]

                data['id_konsultasi'] = new_sesi_konsultasi_id
                data['no_rekam_medis_pasien'] = request.POST.get(
                    'nomor_rekam_medis_pasien')
                data['id_transaksi'] = request.POST.get('id_transaksi')
                data['tanggal'] = request.POST.get('tanggal')
                data['biaya'] = 0
                data['status'] = 'booked'

                if (any(get_sesi_konsultasi(data['id_konsultasi']))):
                    return render(request, 'dashboardAdmin/SesiKonsultasi/buatSesiKonsultasi.html', data)
                else:
                    # print(data)
                    insert_sesi_konsultasi(data)
                return redirect("pages:daftarKonsultasi")
            else:
                data = {}
                data['nomor_rekam_medis'] = get_list_nomor_rekam_medis_pasien()
                data['id_transaksi'] = get_list_id_transaksi()

                return render(request, 'dashboardAdmin/SesiKonsultasi/buatSesiKonsultasi.html', data)
        else:
            return redirect('pages:daftarRsCabang')
    else:
        data = {}
        data['nomor_rekam_medis'] = get_list_nomor_rekam_medis_pasien()

        return render(request, 'dashboardAdmin/SesiKonsultasi/buatSesiKonsultasi.html', data)
        return redirect('user:login')

def daftarKonsultasi(request):
    data = {}
    if request.session.get('user') is not None:
        data['sesi_konsultasi'] = fetch_all_sesi_konsultasi()
        return render(request, 'dashboardAdmin/SesiKonsultasi/daftarSesiKonsultasi.html', data)
    else:
        return redirect('user:login')

def updateKonsultasi(request, id_konsultasi):
    data = {}
    if request.session.get('user') is not None:
        if (request.session['user']['role'] == 'admin'):
            if request.method == "GET":
                if any(get_sesi_konsultasi(id_konsultasi)):
                    data = get_sesi_konsultasi(id_konsultasi)[0]
                    # print(data)
                    return render(request, 'dashboardAdmin/SesiKonsultasi/updateSesiKonsultasi.html', data)
                else:
                    return redirect('pages:daftarKonsultasi')
            else:
                data['id_konsultasi'] = id_konsultasi
                data['tanggal'] = request.POST.get('tanggal')
                data['status'] = request.POST.get('status')

                update_sesi_konsultasi(data)
                return redirect('pages:daftarKonsultasi')
        else:
            return redirect('pages:daftarKonsultasi')
    else:
        return redirect('user:login')


def deleteKonsultasi(request, id_konsultasi):
    if request.session.get('user') is not None:
        if (request.session['user']['role'] == 'admin'):
            if request.method == "POST":
                sesi_konsultasi = get_sesi_konsultasi(id_konsultasi)[0]
                delete_sesi_konsultasi(sesi_konsultasi['id_konsultasi'])
        return redirect('pages:daftarKonsultasi')
    else:
        return redirect('user:login')

# # # # # # # # route poliklinik # # # # # # #

def buatLayananJadwalPoliklinik(request):
    data = {}
    if request.session.get('user') is not None:
        if(request.session['user']['role'] == 'admin') :
            print(request.method)
            if request.method == "POST":

                current_id_poliklinik = get_biggest_id_poliklinik()
                #print("ini")
                current_id_poliklinik_angka = current_id_poliklinik.split('.')
                angka_increment = str(int(current_id_poliklinik_angka[0]) + 1)
                new_id_poliklinik = angka_increment + '.' + current_id_poliklinik_angka[1]
                data['id_poliklinik'] = new_id_poliklinik

                data['nama'] = request.POST.get('nama')
                data['deskripsi'] = request.POST.get('deskripsi')
                data['kode_rs_cabang'] = request.POST.get('kode_rs_cabang')

                print(data)
                insert_layanan_poliklinik(data)
                return redirect("pages:daftarLayananJadwalPoliklinik")
            else:
                data['id_poliklinik'] = fetch_all_id_poliklinik()
                data['nama'] = fetch_all_nama()
                data['deskripsi'] = fetch_all_deskripsi()
                data['kode_rs_cabang'] = fetch_all_kode_rs_cabang()

                return render(request, 'dashboardAdmin/Poliklinik/buatLayananJadwalPoliklinik.html', data)
        else :
            return redirect("pages:daftarLayananJadwalPoliklinik")
    else :
        return redirect('user:login')


def daftarLayananJadwalPoliklinik(request):
    data = {}
    # if request.session.get('user') is not None:
    if request.method == "GET":
        data['layanan_poliklinik'] = fetch_all_layanan_poliklinik()
        data['jadwal_layanan_poliklinik'] = fetch_all_jadwal_layanan_poliklinik()
        print(data)
        return render(request, 'dashboardAdmin/Poliklinik/daftarLayananJadwalPoliklinik.html', data)
    else:
        return redirect('user:login')

def updateLayananJadwalPoliklinik(request, id_poliklinik):
    data = {}
    if request.session.get('user') is not None :
        if (request.session['user']['role']=='admin'):
            if request.method == "GET" :
                if any(get_layanan_poliklinik(id_poliklinik)):
                    data = get_layanan_poliklinik(id_poliklinik)[0]
                    return render(request, 'dashboardAdmin/Poliklinik/updateLayananJadwalPoliklinik.html', data)
                else :
                    return redirect('pages:updateLayananJadwalPoliklinik')
            else:
                data['id_poliklinik'] = id_poliklinik
                data['nama'] = request.POST.get('nama')
                data['deskripsi'] = request.POST.get('deskripsi')
                data['kode_rs_cabang'] = request.POST.get('kode_rs_cabang')

                update_layanan_poliklinik(data)
                return redirect('pages:daftarLayananJadwalPoliklinik')
        else :
            return redirect('pages:daftarLayananJadwalPoliklinik')
    else :
        return redirect('user:login')



def deleteLayananJadwalPoliklinik(request, id_poliklinik):
    data = {}
    if request.session.get('user') is not None:
        if(request.session['user']['role'] == 'admin'):
            if request.method == "POST":
                layanan_poliklinik = get_layanan_poliklinik(id_poliklinik)[0]
                delete_layanan_poliklinik(layanan_poliklinik['id_poliklinik'])

        return redirect("pages:daftarLayananJadwalPoliklinik")
    else:
        return redirect('user:login')

# # # # # # # # route RS # # # # # # #


def buatRsCabang(request):
    data = {}
    if request.session.get('user') is not None:
        if (request.session['user']['role'] == 'admin'):
            if request.method == "POST":
                data['kode_rs'] = request.POST['kode_rs']
                data['nama'] = request.POST['nama']
                data['tanggal_pendirian'] = request.POST['tanggal_pendirian']
                data['jalan'] = request.POST.get('jalan')
                data['nomor'] = request.POST.get('nomor')
                data['kota'] = request.POST['kota']

                if (data['nomor'] == ''):
                    data['nomor'] = 0

                if (any(get_rs_cabang(data['kode_rs']))):
                    return render(request, 'dashboardAdmin/RSCabang/buatRsCabang.html', data)
                else:
                    # print(data)
                    insert_rs_cabang(data)
                    return redirect("pages:daftarRsCabang")
            else:
                return render(request, 'dashboardAdmin/RSCabang/buatRsCabang.html')
        else:
            return redirect('pages:daftarRsCabang')
    else:
        return redirect('user:login')

def daftarRsCabang(request):
    data = {}
    if request.session.get('user') is not None:
        data['rs_cabang'] = fetch_all_rs_cabang()
        return render(request, 'dashboardAdmin/RSCabang/daftarRSCabang.html', data)
    else:
        return redirect('user:login')


def updateRsCabang(request, kode_rs):
    data = {}
    if request.session.get('user') is not None:
        if (request.session['user']['role'] == 'admin'):
            if request.method == "GET":
                print(kode_rs)
                if any(get_rs_cabang(kode_rs)):
                    data = get_rs_cabang(kode_rs)[0]
                    # print(data)
                    return render(request, 'dashboardAdmin/RSCabang/updateRsCabang.html', data)
                else:
                    return redirect('pages:daftarRsCabang')
            else:
                data['kode_rs'] = kode_rs
                data['nama'] = request.POST.get('nama')
                data['tanggal_pendirian'] = request.POST.get('tanggal')
                data['jalan'] = request.POST.get('jalan')
                data['nomor'] = request.POST.get('nomor')
                data['kota'] = request.POST.get('kota')

                update_rs_cabang(data)

                return redirect('pages:daftarRsCabang')
        else:
            return redirect('pages:daftarRsCabang')
    else:
        return redirect('user:login')


def deleteRsCabang(request, kode_rs):
    if request.session.get('user') is not None:
        if (request.session['user']['role'] == 'admin'):
            if request.method == "POST":
                rs_cabang = get_rs_cabang(kode_rs)[0]
                delete_rs_cabang(rs_cabang['kode_rs'])
        return redirect('pages:daftarRsCabang')
    else:
        return redirect('user:login')

def daftarDokterRsCabang(request):
    data = {}
    if request.session.get('user') is not None:
        if(request.session['user']['role'] == 'admin') :
            print(request.method)
            if request.method == "POST":
                data['id_dokter'] = request.POST.get('id_dokter')
                data['kode_rs'] = request.POST.get('kode_rs')

                if (any(get_dokter_rs_cabang(data['id_dokter'],data['kode_rs']))):
                    #print(data)
                    return render(request, 'dashboardAdmin/RSCabang/daftarDokterRSCabang.html', data)
                else:
                    #print(data)
                    insert_dokter_rs_cabang(data)
                    return redirect("pages:listDokterRSCabang")
            else:
                data['id_dokter'] = fetch_all_id_dokter_rs_cabang()
                data['kode_rs'] = fetch_all_kode_rs_dokter_rs_cabang()
                print(data['id_dokter'])
                #print(data)
                return render(request, 'dashboardAdmin/RSCabang/daftarDokterRSCabang.html', data)
        else :
            return redirect("pages:listDokterRSCabang")
    else :
        return redirect('user:login')

def listDokterRsCabang(request):
    data = {}
    if request.method == "GET":
        data['dokter_rs_cabang'] = fetch_all_dokter_rs_cabang()
        return render(request, 'dashboardAdmin/RSCabang/listDokterRSCabang.html', data)
    else:
        return redirect('user:login')

def updateDokterRsCabang(request, id_dokter, kode_rs):
    data = {}
    if request.session.get('user') is not None :
        if (request.session['user']['role']=='admin'):
            if request.method == "GET" :
                if any(get_dokter_rs_cabang(id_dokter, kode_rs)):
                    data = get_dokter_rs_cabang(id_dokter, kode_rs)[0]
                    return render(request, 'dashboardAdmin/RSCabang/updateDokterRSCabang.html', data)
                else :
                    return redirect('pages:listDokterRSCabang')
            else:
                data['id_dokter_current'] = id_dokter
                data['kode_rs_current'] = kode_rs
                data['id_dokter_update'] = request.POST.get('id_dokter')
                data['kode_rs_update'] = request.POST.get('kode_rs')

                update_dokter_rs_cabang(data)
                return redirect('pages:listDokterRSCabang')
        else :
            return redirect('pages:listDokterRSCabang')
    else :
        return redirect('user:login')


def deleteDokterRsCabang(request, id_dokter, kode_rs):
    data = {}
    if request.session.get('user') is not None:
        if(request.session['user']['role'] == 'admin'):
            if request.method == "POST":
                dokter_rs_cabang = get_dokter_rs_cabang(id_dokter, kode_rs)[0]
                print("tes")
                print(dokter_rs_cabang)
                delete_dokter_rs_cabang(dokter_rs_cabang['id_dokter'], dokter_rs_cabang['kode_rs'])

        return redirect("pages:listDokterRSCabang")
    else:
        return redirect('user:login')


# # # # # # # # route tindakan # # # # # # #


def buatTindakan(request):
    data = {}
    if request.session.get('user') is not None:
        if(request.session['user']['role'] == 'admin') :
            print(request.method)
            if request.method == "POST":
                current_no_urut = get_biggest_no_urut()
                #print("ini")
                current_no_urut_angka = current_no_urut.split('.')
                angka_increment = str(int(current_no_urut_angka[0]) + 1)
                new_no_urut = angka_increment + '.' + current_no_urut_angka[1]
                data['no_urut'] = new_no_urut
                print("new no urut =" + new_no_urut)
                data['biaya'] = request.POST.get('biaya')
                data['catatan'] = request.POST.get('catatan')
                data['id_konsultasi'] = request.POST.get('id_konsultasi')
                data['id_transaksi'] = request.POST.get('id_transaksi')
                data['id_tindakan_poli'] = request.POST.get('id_tindakan_poli')

                if (data['biaya'] == None):
                    data['biaya'] = 0

                if (any(get_tindakan(data['no_urut'],data['id_konsultasi']))):
                    #print(data)
                    return render(request, 'dashboardAdmin/Tindakan/buatTindakan.html', data)
                else:
                    #print(data)
                    insert_tindakan(data)
                    return redirect("pages:daftarTindakan")
            else:
                data['id_konsultasi'] = fetch_all_id_konsultasi()
                data['id_transaksi'] = fetch_all_id_transaksi()
                data['id_tindakan_poli'] = fetch_all_id_tindakan_poli()
                print(data['id_konsultasi'])
                #print(data)
                return render(request, 'dashboardAdmin/Tindakan/buatTindakan.html', data)
        else :
            return redirect("pages:daftarTindakan")
    else :
        return redirect('user:login')



def updateTindakan(request, no_urut, id_konsultasi):
    data = {}
    if request.session.get('user') is not None :
        if (request.session['user']['role']=='admin'):
            if request.method == "GET" :
                if any(get_tindakan(no_urut, id_konsultasi)):
                    data = get_tindakan(no_urut, id_konsultasi)[0]
                    return render(request, 'dashboardAdmin/Tindakan/updateTindakan.html', data)
                else :
                    return redirect('pages:daftarTindakan')
            else:
                data['id_konsultasi'] = request.POST.get('id_konsultasi')
                data['no_urut'] = request.POST.get('no_urut')
                data['biaya'] = request.POST.get('biaya')
                data['catatan'] = request.POST.get('catatan')
                data['id_transaksi'] = request.POST.get('id_transaksi')
                data['id_tindakan_poli'] = request.POST.get('id_tindakan_poli')

                update_tindakan(data)
                return redirect('pages:daftarTindakan')
        else :
            return redirect('pages:daftarTindakan')
    else :
        return redirect('user:login')


def deleteTindakan(request, no_urut, id_konsultasi):
    if request.session.get('user') is not None:
        if(request.session['user']['role'] == 'admin'):
            if request.method == "POST":
                tindakan = get_tindakan(no_urut,id_konsultasi)[0]
                delete_tindakan(tindakan['no_urut'], tindakan['id_konsultasi'])
        return redirect("pages:daftarTindakan")
    else:
        return redirect('user:login')


def daftarTindakan(request):
    data = {}
    # if request.session.get('user') is not None:
    if request.session.get('user') is not None:
        data['tindakan'] = fetch_all_tindakan()
        print(data)
        return render(request, 'dashboardAdmin/Tindakan/daftarTindakan.html', data)
    else:
        return redirect('user:login')


def buatTindakanPoliklinik(request):
    data = {}
    print(request.method)
    if request.session.get('user') is not None :
        if(request.session['user']['role'] == 'admin') :

            if request.method == "POST":
                current_itp = get_biggest_itp()
                print("ini")
                current_itp = current_itp.split('.')
                angka_increment = str(int(current_itp[0]) + 1)
                new_itp = angka_increment + '.' + current_itp[1]
                data['id_tindakan_poli'] = new_itp
                print("ini")
                data['id_poliklinik'] = request.POST['id_poliklinik']
                data['nama_tindakan'] = request.POST['nama_tindakan']
                data['tarif'] = request.POST.get('tarif')
                data['deskripsi'] = request.POST.get('deskripsi')

                if (any(get_tindakan_poli(data['id_tindakan_poli']))):
                    return render(request, 'dashboardAdmin/Tindakan/buatTindakanPoliklinik.html', data)
                else:
                    #print(data)
                    #print(tes)
                    insert_tindakan_poli(data)
                    return redirect("pages:daftarTindakanPoliklinik")
            else:
                data['id_poliklinik'] = fetch_all_id_poliklinik()
                #print(data['id_poliklinik'])
                return render(request, 'dashboardAdmin/Tindakan/buatTindakanPoliklinik.html', data)
        else :
            return render(request, 'dashboardAdmin/Tindakan/buatTindakanPoliklinik', data)
    else :
        return redirect('user:login')



def daftarTindakanPoliklinik(request):
    data = {}
    if request.session.get('user') is not None :
        data['tindakan_poli'] = fetch_all_tindakan_poli()
        print(data)
        return render(request, 'dashboardAdmin/Tindakan/daftarTindakanPoliklinik.html', data)
    else:
        return redirect('user:login')

def updateTindakanPoliklinik(request, id_tindakan_poli):
    data = {}
    if request.session.get('user') is not None :
        if(request.session['user']['role'] == 'admin') :
            if request.method == "GET":
                if any(get_tindakan_poli(id_tindakan_poli)):
                    data = get_tindakan_poli(id_tindakan_poli)[0]
                    return render(request, 'dashboardAdmin/Tindakan/updateTindakanPoliklinik.html', data)
                else :
                    return redirect('pages:daftarTindakanPoliklinik')
            else:
                data['id_tindakan_poli'] = request.POST.get('id_tindakan_poli')
                data['id_poliklinik'] = request.POST.get('id_poliklinik')
                data['nama_tindakan'] = request.POST.get('nama_tindakan')
                data['deskripsi'] = request.POST.get('deskripsi')
                data['tarif'] = request.POST.get('tarif')

                update_tindakan_poli(data)
                return redirect('pages:daftarTindakanPoliklinik')
        else:
            return redirect('pages:daftarTindakanPoliklinik')

    else :
        return redirect('user:login')

def deleteTindakanPoliklinik(request, id_tindakan_poli):
    if request.session.get('user') is not None:
        if(request.session['user']['role'] == 'admin'):
            if request.method == "POST":
                tindakan_poli = get_tindakan_poli(id_tindakan_poli)[0]
                print(id_tindakan_poli)
                delete_tindakan_poli(tindakan_poli['id_tindakan_poli'])
        return redirect("pages:daftarTindakanPoliklinik")
    else:
        return redirect('user:login')

# # # # # # # # route transaksi # # # # # # #
def buatTransaksi(request):
    data = {}
    today = date.today()

    if request.session.get('user') is not None:
        if (request.session['user']['role'] == 'admin'):
            if request.method == "POST":
                current_id = get_biggest_transaksi_id()
                current_id_angka = current_id.split('.')
                angka_increment = str(int(current_id_angka[0]) + 1)
                new_transaksi_id = angka_increment + '.' + current_id_angka[1]

                data['id_transaksi'] = new_transaksi_id
                data['tanggal'] = today.strftime("%B %d, %Y")
                data['status'] = 'Created'
                data['total_biaya'] = 0
                data['waktu_pembayaran'] = today.strftime("%B %d, %Y, %H:%M")
                data['no_rekam_medis'] = request.POST.get('no_rekam_medis')

                if (any(get_transaksi(data['id_transaksi']))):
                    return render(request, 'dashboardAdmin/Transaksi/buatTransaksi.html', data)
                else:
                    # print(data)
                    insert_transaksi(data)
                return redirect("pages:daftarTransaksi")
            else:
                data = {}
                data['no_rekam_medis'] = get_list_nomor_rekam_medis_pasien()

                return render(request, 'dashboardAdmin/Transaksi/buatTransaksi.html', data)
        else:
            return redirect('pages:daftarTransaksi')
    else:
        return redirect('user:login')


def daftarTransaksi(request):
    data = {}
    # if request.session.get('user') is not None:
    if request.method == "GET":
        data['transaksi'] = fetch_all_transaksi()
        print(data)
        return render(request, 'dashboardAdmin/Transaksi/daftarTransaksi.html', data)
    else:
        return redirect('user:login')


def updateTransaksi(request, id_transaksi):
    data = {}
    if request.session.get('user') is not None:
        if (request.session['user']['role'] == 'admin'):
            if request.method == "GET":
                print(id_transaksi)
                if any(get_transaksi(id_transaksi)):
                    data = get_transaksi(id_transaksi)[0]
                    # print(data)
                    return render(request, 'dashboardAdmin/Transaksi/updateTransaksi.html', data)
                else:
                    return redirect('pages:daftarTransaksi')
            else:
                data['id_transaksi'] = id_transaksi
                data['tanggal'] = request.POST.get('tanggal')
                data['status'] = request.POST.get('status')
                data['total_biaya'] = request.POST.get('total_biaya')
                data['waktu_pembayaran'] = request.POST.get('waktu_pembayaran')
                data['no_rekam_medis'] = request.POST.get('no_rekam_medis')

                update_transaksi(data)

                return redirect('pages:daftarTransaksi')
        else:
            return redirect('pages:daftarTransaksi')
    else:
        return redirect('user:login')


def deleteTransaksi(request, id_transaksi):
    if request.session.get('user') is not None:
        if (request.session['user']['role'] == 'admin'):
            if request.method == "POST":
                transaksi = get_transaksi(id_transaksi)[0]
                delete_transaksi(transaksi['id_transaksi'])
        return redirect("pages:daftarTransaksi")
    else:
        return redirect('user:login')
