from django.db import models, connection
from .converter import dictfetchall

#### konsultasi ####


def fetch_all_sesi_konsultasi():
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM SESI_KONSULTASI")
    return dictfetchall(cursor)


def get_biggest_konsultasi_id():
    cursor = connection.cursor()
    cursor.execute(
        "SELECT * FROM SESI_KONSULTASI ORDER BY CAST(id_konsultasi AS DOUBLE PRECISION) DESC LIMIT 1")
    produk = cursor.fetchone()
    if (produk is None):
        return '0.0'
    else:
        return produk[0]


def get_list_nomor_rekam_medis_pasien():
    cursor = connection.cursor()
    cursor.execute("SELECT no_rekam_medis FROM pasien")
    return dictfetchall(cursor)


def get_list_id_transaksi():
    cursor = connection.cursor()
    cursor.execute("SELECT id_transaksi FROM transaksi")
    return dictfetchall(cursor)


def get_sesi_konsultasi(id_konsultasi):
    cursor = connection.cursor()
    cursor.execute(
        "SELECT * FROM SESI_KONSULTASI WHERE id_konsultasi = %s", [id_konsultasi])
    return dictfetchall(cursor)


def insert_sesi_konsultasi(data):
    cursor = connection.cursor()
    cursor.execute("INSERT INTO SESI_KONSULTASI VALUES (%s, %s, %s, %s, %s, %s)",
                   [
                       data['id_konsultasi'], data['no_rekam_medis_pasien'], data['tanggal'], data['biaya'], data['status'], data['id_transaksi']
                   ])
    return


def update_sesi_konsultasi(data):
    cursor = connection.cursor()
    cursor.execute("UPDATE SESI_KONSULTASI SET " +
                   "tanggal = %s, status = %s " +
                   "WHERE id_konsultasi = %s",
                   [
                       data['tanggal'], data['status'], data['id_konsultasi']
                   ])


def delete_sesi_konsultasi(id_konsultasi):
    cursor = connection.cursor()
    cursor.execute(
        "DELETE FROM SESI_KONSULTASI WHERE id_konsultasi= %s", [id_konsultasi])

#### poliklinik ####

#### rs ####


def fetch_all_rs_cabang():
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM RS_CABANG")
    return dictfetchall(cursor)


def get_biggest_rs_id():
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM RS_CABANG ORDER BY kode_rs DESC LIMIT 1")
    produk = cursor.fetchone()
    if (produk is None):
        return '0.0'
    else:
        return produk[0]


def get_rs_cabang(kode_rs):
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM RS_CABANG WHERE kode_rs = %s", [kode_rs])
    return dictfetchall(cursor)


def insert_rs_cabang(data):
    cursor = connection.cursor()
    cursor.execute("INSERT INTO RS_CABANG VALUES (%s, %s, %s, %s, %s, %s)",
                   [
                       data['kode_rs'], data['nama'], data['tanggal_pendirian'], data['jalan'], data['nomor'], data['kota']
                   ])
    return


def update_rs_cabang(data):
    cursor = connection.cursor()
    cursor.execute("UPDATE RS_CABANG SET " +
                   "nama = %s, tanggal_pendirian = %s, jalan = %s, nomor = %s, kota = %s" +
                   "WHERE kode_rs = %s",
                   [
                       data['nama'], data['tanggal_pendirian'], data['jalan'], data['nomor'], data['kota'], data['kode_rs']
                   ])


def delete_rs_cabang(kode_rs):
    cursor = connection.cursor()
    cursor.execute("DELETE FROM RS_CABANG WHERE kode_rs= %s", [kode_rs])

#### tindakan ####


def fetch_all_tindakan():
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM TINDAKAN")
    return dictfetchall(cursor)


def fetch_all_id_konsultasi():
    cursor = connection.cursor()
    cursor.execute("SELECT id_konsultasi FROM SESI_KONSULTASI")
    return dictfetchall(cursor)


def fetch_all_id_poliklinik():
    cursor = connection.cursor()
    cursor.execute("SELECT id_poliklinik FROM LAYANAN_POLIKLINIK")
    return dictfetchall(cursor)


def fetch_all_id_transaksi():
    cursor = connection.cursor()
    cursor.execute("SELECT id_transaksi FROM TRANSAKSI")
    return dictfetchall(cursor)


def fetch_all_id_tindakan_poli():
    cursor = connection.cursor()
    cursor.execute("SELECT id_tindakan_poli FROM TINDAKAN_POLI")
    return dictfetchall(cursor)


def get_biggest_no_urut():
    cursor = connection.cursor()
    cursor.execute(
        "SELECT * FROM TINDAKAN ORDER BY CAST(no_urut AS DOUBLE PRECISION) DESC LIMIT 1")
    produk = cursor.fetchone()
    if (produk is None):
        return '0.0'
    else:
        return produk[0]


def get_biggest_itp():
    cursor = connection.cursor()
    cursor.execute(
        "SELECT * FROM TINDAKAN_POLI ORDER BY CAST(id_tindakan_poli AS DOUBLE PRECISION) DESC LIMIT 1")
    produk = cursor.fetchone()
    if (produk is None):
        return '0.0'
    else:
        return produk[0]


def get_tindakan(no_urut, id_konsultasi):
    cursor = connection.cursor()
    cursor.execute(
        "SELECT * FROM TINDAKAN WHERE no_urut = %s AND id_konsultasi = %s", [no_urut, id_konsultasi])
    return dictfetchall(cursor)


def insert_tindakan(data):
    cursor = connection.cursor()
    cursor.execute("INSERT INTO TINDAKAN VALUES (%s, %s, %s, %s, %s)",
                   [
                       data['id_konsultasi'], data['no_urut'], data['biaya'], data['catatan'], data['id_transaksi']
                   ])
    return


def update_tindakan(data):
    cursor = connection.cursor()
    cursor.execute("UPDATE TINDAKAN SET " +
                   "biaya = %s, catatan = %s, id_transaksi = %s" +
                   "WHERE id_konsultasi = %s AND no_urut = %s",
                   [
                       data['biaya'], data['catatan'], data['id_transaksi'], data['id_konsultasi'], data['no_urut']
                   ])


def delete_tindakan(no_urut, id_konsultasi):
    cursor = connection.cursor()
    cursor.execute("DELETE FROM TINDAKAN WHERE no_urut = %s AND id_konsultasi = %s ", [
                   no_urut, id_konsultasi])


#### Tindakan Poliklinik ####
def fetch_all_tindakan_poli():
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM TINDAKAN_POLI")
    return dictfetchall(cursor)


def get_biggest_tindakan_poli_id():
    cursor = connection.cursor()
    cursor.execute(
        "SELECT * FROM TINDAKAN_POLI ORDER BY id_tindakan_poli DESC LIMIT 1")
    produk = cursor.fetchone()
    return produk[0]


def get_tindakan_poli(id_tindakan_poli):
    cursor = connection.cursor()
    cursor.execute(
        "SELECT * FROM TINDAKAN_POLI WHERE id_tindakan_poli = %s", [id_tindakan_poli])
    return dictfetchall(cursor)


def insert_tindakan_poli(data):
    cursor = connection.cursor()
    cursor.execute("INSERT INTO TINDAKAN_POLI VALUES (%s, %s, %s, %s, %s)",
                   [
                       data['id_tindakan_poli'], data['id_poliklinik'], data['nama_tindakan'], data['tarif'], data['deskripsi']
                   ])
    return


def update_tindakan_poli(data):
    cursor = connection.cursor()
    cursor.execute("UPDATE TINDAKAN_POLI SET " +
                   "id_poliklinik = %s, nama_tindakan = %s, tarif = %s, deskripsi = %s" +
                   "WHERE id_tindakan_poli = %s",
                   [
                       data['id_poliklinik'], data['nama_tindakan'], data['tarif'], data['deskripsi'], data['id_tindakan_poli']
                   ])


def delete_tindakan_poli(id_tindakan_poli):
    cursor = connection.cursor()
    cursor.execute("DELETE FROM TINDAKAN_POLI WHERE id_tindakan_poli = %s", [
                   id_tindakan_poli])


#### transaksi ####
def get_biggest_transaksi_id():
    cursor = connection.cursor()
    cursor.execute(
        "SELECT * FROM TRANSAKSI ORDER BY CAST(id_transaksi AS DOUBLE PRECISION) DESC LIMIT 1")
    transaksi = cursor.fetchone()
    if (transaksi is None):
        return '0.0'
    else:
        return transaksi[0]


def fetch_all_transaksi():
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM TRANSAKSI")
    return dictfetchall(cursor)


def fetch_nomor_rekam_medis_pasien():
    cursor = connection.cursor()
    cursor.execute("SELECT no_rekam_medis FROM PASIEN")
    return dictfetchall(cursor)


def get_transaksi(id_transaksi):
    cursor = connection.cursor()
    cursor.execute(
        "SELECT * FROM TRANSAKSI WHERE id_transaksi = %s", [id_transaksi])
    return dictfetchall(cursor)


def insert_transaksi(data):
    cursor = connection.cursor()
    cursor.execute("INSERT INTO TRANSAKSI VALUES (%s, %s, %s, %s, %s, %s)",
                   [
                       data['id_transaksi'], data['tanggal'], data['status'], data['total_biaya'], data['waktu_pembayaran'], data['no_rekam_medis']
                   ])
    return


def update_transaksi(data):
    cursor = connection.cursor()
    cursor.execute("UPDATE TRANSAKSI SET " +
                   "tanggal = %s, status = %s, waktu_pembayaran = %s" +
                   "WHERE id_transaksi = %s",
                   [
                       data['id_transaksi'], data['tanggal'], data['status'], data['total_biaya'], data['waktu_pembayaran'], data['no_rekam_medis']
                   ])


def delete_transaksi(id_transaksi):
    cursor = connection.cursor()
    cursor.execute(
        "DELETE FROM TRANSAKSI WHERE id_transaksi= %s", [id_transaksi])

#### dokter rs cabang ####


def fetch_all_dokter_rs_cabang():
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM DOKTER_RS_CABANG")
    return dictfetchall(cursor)


def fetch_all_id_dokter_rs_cabang():
    cursor = connection.cursor()
    cursor.execute("SELECT id_dokter FROM dokter_rs_cabang")
    return dictfetchall(cursor)


def fetch_all_kode_rs_dokter_rs_cabang():
    cursor = connection.cursor()
    cursor.execute("SELECT kode_rs FROM dokter_rs_cabang")
    return dictfetchall(cursor)


def get_biggest_id_dokter():
    cursor = connection.cursor()
    cursor.execute(
        "SELECT * FROM DOKTER_RS_CABANG ORDER BY id_dokter DESC LIMIT 1")
    produk = cursor.fetchone()
    return produk[0]


def get_dokter_rs_cabang(id_dokter, kode_rs):
    cursor = connection.cursor()
    cursor.execute(
        "SELECT * FROM DOKTER_RS_CABANG WHERE id_dokter = %s AND kode_rs = %s", [id_dokter, kode_rs])
    return dictfetchall(cursor)


def insert_dokter_rs_cabang(data):
    cursor = connection.cursor()
    cursor.execute("INSERT INTO DOKTER_RS_CABANG VALUES (%s, %s)",
                   [
                       data['id_dokter'], data['kode_rs']
                   ])
    return


def update_dokter_rs_cabang(data):
    cursor = connection.cursor()
    cursor.execute("UPDATE DOKTER_RS_CABANG SET " +
                   "id_dokter = %s, kode_rs = %s " +
                   "WHERE id_dokter = %s AND kode_rs = %s",
                   [
                       data['id_dokter_update'], data['kode_rs_update'], data['id_dokter_current'], data['kode_rs_current']
                   ])
    return


def delete_dokter_rs_cabang(id_dokter, kode_rs):
    cursor = connection.cursor()
    cursor.execute("DELETE FROM DOKTER_RS_CABANG WHERE id_dokter= %s AND kode_rs = %s", [
                   id_dokter, kode_rs])
    return


def get_list_id_dokter_rs_cabang():
    cursor = connection.cursor()
    cursor.execute("SELECT id_dokter FROM dokter_rs_cabang")
    return dictfetchall(cursor)


def get_list_id_kode_dokter_rs_cabang():
    cursor = connection.cursor()
    cursor.execute("SELECT kode_rs FROM dokter_rs_cabang")
    return dictfetchall(cursor)

### layanan poliklinik dan jadwal ###


def fetch_all_layanan_poliklinik():
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM LAYANAN_POLIKLINIK")
    return dictfetchall(cursor)


def fetch_all_jadwal_layanan_poliklinik():
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM JADWAL_LAYANAN_POLIKLINIK")
    return dictfetchall(cursor)


def get_biggest_id_poliklinik():
    cursor = connection.cursor()
    cursor.execute(
        "SELECT * FROM LAYANAN_POLIKLINIK ORDER BY id_poliklinik DESC LIMIT 1")
    produk = cursor.fetchone()
    return produk[0]


def get_biggest_id_jadwal_poliklinik():
    cursor = connection.cursor()
    cursor.execute(
        "SELECT * FROM JADWAL_LAYANAN_POLIKLINIK ORDER BY id_jadwal_poliklinik DESC LIMIT 1")
    produk = cursor.fetchone()
    return produk[0]


def get_layanan_poliklinik(id_poliklinik):
    cursor = connection.cursor()
    cursor.execute(
        "SELECT * FROM LAYANAN_POLIKLINIK WHERE id_poliklinik = %s", [id_poliklinik])
    return dictfetchall(cursor)


def get_jadwal_layanan_poliklinik(id_jadwal_poliklinik):
    cursor = connection.cursor()
    cursor.execute(
        "SELECT * FROM JADWAL_LAYANAN_POLIKLINIK WHERE id_jadwal_poliklinik = %s", [id_jadwal_poliklinik])
    return dictfetchall(cursor)


def insert_layanan_poliklinik(data):
    cursor = connection.cursor()
    cursor.execute("INSERT INTO LAYANAN_POLIKLINIK VALUES (%s, %s, %s, %s)",
                   [
                       data['id_poliklinik'], data['kode_rs_cabang'], data['nama'], data['deskripsi']
                   ])
    return


def insert_jadwal_layanan_poliklinik(data):
    cursor = connection.cursor()
    cursor.execute("INSERT INTO JADWAL_LAYANAN_POLIKLINIK VALUES (%s, %s, %s, %s, %s, %s, %s)",
                   [
                       data['id_jadwal_poliklinik'], data['waktu_mulai'], data['waktu_selesai'], data[
                           'hari'], data['kapasitas'], data['id_dokter'], data['id_poliklinik']
                   ])
    return


def update_layanan_poliklinik(data):
    cursor = connection.cursor()
    cursor.execute("UPDATE LAYANAN_POLIKLINIK SET " +
                   "kode_rs_cabang = %s, nama = %s, deskripsi = %s" +
                   "WHERE id_poliklinik = %s",
                   [
                       data['kode_rs_cabang'], data['nama'], data['deskripsi'], data['id_poliklinik']
                   ])


def update_jadwal_layanan_poliklinik(data):
    cursor = connection.cursor()
    cursor.execute("UPDATE JADWAL_LAYANAN_POLIKLINIK SET " +
                   "id_jadwal_poliklinik = %s, waktu_mulai = %s, waktu_selesai = %s, hari = %s, kapasitas = %s, id_dokter = %s, id_poliklinik = %s" +
                   "WHERE id_jadwal_poliklinik = %s",
                   [
                       data['id_jadwal_poliklinik'], data['waktu_mulai'], data['waktu_selesai'], data[
                           'hari'], data['kapasitas'], data['id_dokter'], data['id_poliklinik']
                   ])


def delete_layanan_poliklinik(id_poliklinik):
    cursor = connection.cursor()
    cursor.execute(
        "DELETE FROM LAYANAN_POLIKLINIK WHERE id_poliklinik= %s", [id_poliklinik])


def delete_jadwal_layanan_poliklinik(id_jadwal_poliklinik):
    cursor = connection.cursor()
    cursor.execute("DELETE FROM JADWAL_LAYANAN_POLIKLINIK WHERE id_jadwal_poliklinik= %s", [
                   id_jadwal_poliklinik])


# def fetch_all_id_poliklinik():
#     cursor = connection.cursor()
#     cursor.execute("SELECT id_poliklinik FROM LAYANAN_POLIKLINIK")
#     return dictfetchall(cursor)


def fetch_all_nama():
    cursor = connection.cursor()
    cursor.execute("SELECT nama FROM LAYANAN_POLIKLINIK")
    return dictfetchall(cursor)


def fetch_all_deskripsi():
    cursor = connection.cursor()
    cursor.execute("SELECT deskripsi FROM LAYANAN_POLIKLINIK")
    return dictfetchall(cursor)


def fetch_all_kode_rs_cabang():
    cursor = connection.cursor()
    cursor.execute("SELECT kode_rs_cabang FROM LAYANAN_POLIKLINIK")
    return dictfetchall(cursor)
